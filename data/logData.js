var fs = require("fs");

fs.readFile("logData.json", "utf-8", function(err, data) {
    if(err) {
        console.log(err);
    } else {
        data = JSON.parse(data);
        
        console.log("Unique Clicks: " + data["uniqueClicks"]);
        console.log("Overall Clicks: " + data["clicks"]);
        console.log("Users: " + data["users"]);
    }
});
var express = require("express"),
    app = express(),
    http = require("http"),
    bodyParser = require("body-parser"),
    fs = require("fs"),
    dateformat = require("dateformat");
    
app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended: true}));

app.get("/", function (req, res) {
    res.render("home.ejs", {active: "home"});
    
    log(req, "/");
});

app.get("/contact", function(req, res) {
   res.render("contact.ejs", {active: "contact"});
   
    log(req, "/contact");
});

app.get("/about", function(req, res) {
   res.render("about.ejs", {active: "about"});
   
    log(req, "/about");
});

app.get("/impressum", function(req, res) {
    res.render("impressum.ejs", {active: "impressum"});
    
    log(req, "/impressum");
});

app.get("/datenschutz", function(req, res) {
    res.render("datenschutz.ejs", {active: "datenschutz"});
    
    log(req, "/datenschutz");
});

app.listen(process.env.PORT, process.env.IP, function(err) {
    if(err) {
        console.log(err);
    } else {
        console.log("Listening...");
    }
});

// OWN SHIT
function log(req, route) {
    var clientIp = getClientIp(req);
    
    fs.readFile("data/logData.json", "utf-8", function(err, data) {
        if(err) {
            console.log("parsing error!!!!!!!!!");
        } else {
            var logData = JSON.parse(data);
            
            if(!logData["users"].includes(clientIp)) {
                logData["users"].push(clientIp);
                logData["uniqueClicks"] += 1;
            }
            
            logData["clicks"] += 1;
            
            fs.writeFile("data/logData.json", JSON.stringify(logData), "utf-8", function(err) {
               if(err) {
                   console.log(err);
               } else {
                   //success
               }
            });
            
            var text = "[ " + getDatetime() + " ]: " + "unique Clicks: " + logData["uniqueClicks"] + " --- " + clientIp + " at " + route;
            appendFile("data/log.txt", text);
        }
    });
}

function getClientIp(req) {
    var ipAddress;
    var forwardedIpsStr = req.header('x-forwarded-for'); 
    if (forwardedIpsStr) {
        // client, proxy1, proxy2, ...
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
};

function appendFile(path, text) {
    fs.readFile(path, function(err, data) {
        if(err) {
            console.log(err);
        } else {
            fs.writeFile(path, data + "\n" + text, "utf-8", function(err) {
               if(err) {
                   console.log(err);
               } else {
                    console.log(text);
               }
            });
        }
    });
}

function getDatetime() {
    return dateformat(new Date(), "dd-mm-yyyy HH:MM");
}
